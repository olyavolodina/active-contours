import numpy as np
import active_contours
import utils
import matplotlib.pyplot as plt

#active_contours.main()

my_mask = plt.imread("nucleus_result.png")
normal_mask = plt.imread("nucleus_mask.png")

overlap = my_mask * normal_mask # Logical AND
union = my_mask + normal_mask # Logical OR
union[union == 2] = 1
IOU = overlap.sum()/float(union.sum())
print(IOU)

