from scipy.linalg import lu
from scipy.ndimage.filters import gaussian_filter, gaussian_gradient_magnitude
import numpy as np
import utils
from scipy.interpolate import interp2d, interp1d
from cv2 import imread, imwrite
import matplotlib.pyplot as plt
import sys


def get_image(imgpath, kernelsize=-1, mode=" "):
    img = plt.imread(imgpath)
    return img

def make_A(a, b, shape):
    A = np.zeros((shape, shape))
    A += np.eye(shape) * (2 * a + 6 * b)
    A += np.eye(shape, k=1) * (-a - 4 * b)
    A += np.eye(shape, k=-1) * (-a - 4 * b)
    A += np.eye(shape, k=2) * b
    A += np.eye(shape, k=-2) * b
    A += np.eye(shape, k=shape-2) * b
    A += np.eye(shape, k=-(shape-2)) * b
    A += np.eye(shape, k=(shape-1)) * (-a - 4 * b)
    A += np.eye(shape, k=-(shape-1)) * (-a - 4 * b)

    return A

def make_F(img, w_line, w_edge, X, balloon_force):

    P_edge = - (gaussian_gradient_magnitude(img, 10))

    P_line = - (gaussian_filter(img, 3))
    P = - w_line * P_line - w_edge * P_edge

    F_x, F_y = np.gradient(P)
    F_x = - F_x
    F_y = - F_y
    intensity = (np.sum(F_x**2 + F_y**2))**.5
    F_x = 20 * F_x / intensity
    F_y = 20 * F_y / intensity


    #plt.imshow(F_x, cmap=cmap)
    #plt.imshow(F_y, cmap=cmap)

    x = range(F_x.shape[0])
    y = range(F_x.shape[1])
    f_x = interp2d(y, x, F_x)
    f_y = interp2d(y, x, F_y)



    #utils.display_snake(F_y, init, X)
    #utils.display_snake(F_y, init, X)

    F = np.zeros(X.shape)
    normal_y = -np.diff(X[:, 0])
    normal_x = np.diff(X[:, 1])
    normal_intens = (normal_y**2 + normal_x**2)**.5
    normal_y /= normal_intens
    normal_x /= normal_intens

    for i in range(F.shape[0]):

        F[i, 0] = f_x(X[i, 1], X[i, 0]) + balloon_force * normal_x[i-1]
        F[i, 1] = f_y(X[i, 1], X[i, 0]) + balloon_force * normal_y[i-1]

    return  F




def reparametrization(X):
    t = range(X.shape[0])


    dr = (np.diff(X[:, 0])**2 + np.diff(X[:, 1])**2)**.5 # segment lengths
    r = np.zeros_like(X[:, 0])
    r[1:] = np.cumsum(dr) # integrate path
    r_int = np.linspace(0, r.max(), X.shape[0]) # regular spaced path
    X[:, 0] = np.interp(r_int, r, X[:, 0]) # interpolate
    X[:, 1] = np.interp(r_int, r, X[:, 1])

def main():
    path = sys.argv[1]
    path_snake = sys.argv[2]
    t = float(sys.argv[6])
    w_line = float(sys.argv[7])
    w_edge = float(sys.argv[8])
    balloon_force = float(sys.argv[9])
    #w_line = 2
    #w_edge = 1
    #t = 0.5
    #path = "nucleus.png"
    img = get_image(path)



    X = np.loadtxt(path_snake)
    h = np.average((np.diff(X[:, 0])**2 + np.diff(X[:, 1])**2)**.5)
    a = 1/h**2
    b = 1/h**4


    init = X
    A = make_A(a, b, X.shape[0])
    matrix = np.eye(X.shape[0]) + A * t
    i = 0
    inverted = np.linalg.inv(matrix)
    while(True):
        i += 1
        F = make_F(img, w_line, w_edge, X, balloon_force)
        X1 = inverted @ ( X + F * t)

        if i % 5 == 0:
            reparametrization(X1)

        if i == 500:
            break
        X = X1

    utils.save_mask(sys.argv[3], X, img)


cmap = 'Greys_r'
if __name__ == "__main__":
    main()








